# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140520021839) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: true do |t|
    t.integer  "user_id",                     null: false
    t.integer  "server_id",                   null: false
    t.string   "account_key",                 null: false
    t.boolean  "validated",   default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "cancellation_reasons", force: true do |t|
    t.text     "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "enchants", force: true do |t|
    t.string   "name",        null: false
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "item_listing_statuses", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "item_listing_types", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "item_listings", force: true do |t|
    t.integer  "user_id",                            null: false
    t.integer  "account_id",                         null: false
    t.integer  "item_id",                            null: false
    t.integer  "quantity",               default: 1
    t.integer  "starting_bid"
    t.integer  "bid_increment",          default: 1
    t.integer  "current_bid"
    t.integer  "buyout"
    t.date     "expiration_date"
    t.integer  "item_listing_type_id",               null: false
    t.integer  "item_listing_status_id", default: 0, null: false
    t.integer  "cancellation_reason_id"
    t.integer  "high_bidder_user_id"
    t.integer  "high_bidder_account_id"
    t.integer  "consumer_user_id"
    t.integer  "consumer_account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "item_listings", ["account_id"], name: "index_item_listings_on_account_id", using: :btree
  add_index "item_listings", ["item_id"], name: "index_item_listings_on_item_id", using: :btree
  add_index "item_listings", ["item_listing_status_id"], name: "index_item_listings_on_item_listing_status_id", using: :btree
  add_index "item_listings", ["item_listing_type_id"], name: "index_item_listings_on_item_listing_type_id", using: :btree
  add_index "item_listings", ["user_id"], name: "index_item_listings_on_user_id", using: :btree

  create_table "item_metrics", force: true do |t|
    t.integer  "item_id"
    t.integer  "item_listing_id"
    t.integer  "seller_user_id"
    t.integer  "seller_account_id"
    t.integer  "buyer_user_id"
    t.integer  "buyer_account_id"
    t.integer  "purchase_price"
    t.integer  "purchase_quantity"
    t.boolean  "listed_wts"
    t.boolean  "listed_wtb"
    t.boolean  "listed_wtt"
    t.boolean  "purchased_bid"
    t.boolean  "purchased_buy"
    t.boolean  "delivered"
    t.boolean  "received"
    t.boolean  "cancelled"
    t.boolean  "expired"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "item_metrics", ["item_id"], name: "index_item_metrics_on_item_id", using: :btree
  add_index "item_metrics", ["item_listing_id"], name: "index_item_metrics_on_item_listing_id", using: :btree

  create_table "item_subtypes", force: true do |t|
    t.string   "name",                        null: false
    t.integer  "item_type_id",                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "enabled",      default: true, null: false
  end

  add_index "item_subtypes", ["item_type_id"], name: "index_item_subtypes_on_item_type_id", using: :btree

  create_table "item_types", force: true do |t|
    t.string   "name",                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "enabled",    default: true, null: false
  end

  create_table "items", force: true do |t|
    t.string   "name",                  null: false
    t.integer  "item_type_id",          null: false
    t.integer  "item_subtype_id",       null: false
    t.integer  "level"
    t.integer  "veteran_level"
    t.integer  "rarity_id"
    t.integer  "style_id"
    t.integer  "set_bonus_trait_id"
    t.integer  "researchable_trait_id"
    t.integer  "enchant_1_id"
    t.integer  "enchant_2_id"
    t.integer  "user_id",               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "enchant_1_value"
    t.integer  "enchant_1_value_2"
    t.integer  "enchant_2_value"
    t.integer  "enchant_2_value_2"
    t.integer  "damage"
    t.integer  "armor"
  end

  add_index "items", ["item_subtype_id"], name: "index_items_on_item_subtype_id", using: :btree
  add_index "items", ["item_type_id"], name: "index_items_on_item_type_id", using: :btree
  add_index "items", ["rarity_id"], name: "index_items_on_rarity_id", using: :btree
  add_index "items", ["researchable_trait_id"], name: "index_items_on_researchable_trait_id", using: :btree
  add_index "items", ["set_bonus_trait_id"], name: "index_items_on_set_bonus_trait_id", using: :btree
  add_index "items", ["style_id"], name: "index_items_on_style_id", using: :btree
  add_index "items", ["user_id"], name: "index_items_on_user_id", using: :btree

  create_table "rarities", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "researchable_traits", force: true do |t|
    t.string   "name",        null: false
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "servers", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "set_bonus_traits", force: true do |t|
    t.string   "name",        null: false
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "styles", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_bids", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "account_id",      null: false
    t.integer  "item_listing_id", null: false
    t.integer  "quantity"
    t.integer  "bid_amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_bids", ["account_id"], name: "index_user_bids_on_account_id", using: :btree
  add_index "user_bids", ["item_listing_id"], name: "index_user_bids_on_item_listing_id", using: :btree
  add_index "user_bids", ["user_id"], name: "index_user_bids_on_user_id", using: :btree

  create_table "user_deliveries", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "account_id",      null: false
    t.integer  "item_listing_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_deliveries", ["account_id"], name: "index_user_deliveries_on_account_id", using: :btree
  add_index "user_deliveries", ["item_listing_id"], name: "index_user_deliveries_on_item_listing_id", using: :btree
  add_index "user_deliveries", ["user_id"], name: "index_user_deliveries_on_user_id", using: :btree

  create_table "user_offers", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "account_id",      null: false
    t.integer  "item_listing_id", null: false
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_offers", ["account_id"], name: "index_user_offers_on_account_id", using: :btree
  add_index "user_offers", ["item_listing_id"], name: "index_user_offers_on_item_listing_id", using: :btree
  add_index "user_offers", ["user_id"], name: "index_user_offers_on_user_id", using: :btree

  create_table "user_purchases", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "account_id",      null: false
    t.integer  "item_listing_id", null: false
    t.integer  "quantity"
    t.integer  "cost"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_purchases", ["account_id"], name: "index_user_purchases_on_account_id", using: :btree
  add_index "user_purchases", ["item_listing_id"], name: "index_user_purchases_on_item_listing_id", using: :btree
  add_index "user_purchases", ["user_id"], name: "index_user_purchases_on_user_id", using: :btree

  create_table "user_receiveds", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "account_id",      null: false
    t.integer  "item_listing_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_receiveds", ["account_id"], name: "index_user_receiveds_on_account_id", using: :btree
  add_index "user_receiveds", ["item_listing_id"], name: "index_user_receiveds_on_item_listing_id", using: :btree
  add_index "user_receiveds", ["user_id"], name: "index_user_receiveds_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "residence_country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin"
    t.boolean  "premium"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
