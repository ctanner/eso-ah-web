users = [
  admin = User.create!(email: "admin@esoloot.com", password: "Password1", confirmed_at: 1.hour.ago, admin: true),
  chris = User.create!(email: "nescient@esoloot.com", password: "Password1", confirmed_at: 1.hour.ago, admin: false),
  rick = User.create!(email: "wykkyd.gaming@gmail.com", password: "m3n@C!ng", confirmed_at: 1.hour.ago, admin: false),
  eu1 = User.create!(email: "eu1@esoloot.com", password: "Password1", confirmed_at: 1.hour.ago, admin: false),
  eu2 = User.create!(email: "eu2@esoloot.com", password: "Password1", confirmed_at: 1.hour.ago, admin: false),
  eu3 = User.create!(email: "eu3@esoloot.com", password: "Password1", confirmed_at: 1.hour.ago, admin: false),
]

servers = Server.create!([
  {name: "NA"},
  {name: "EU"}
  ])

Account.create!([
  {user: admin, server: servers[0], account_key: "@Admin"},
  {user: chris, server: servers[0], account_key: "@Chris"},
  {user: chris, server: servers[1], account_key: "@ChrisEU"},
  {user: rick, server: servers[0], account_key: "@Wykkyd"},
  {user: eu1, server: servers[1], account_key: "@EU1"},
  {user: eu2, server: servers[1], account_key: "@EU2"},
  {user: eu3, server: servers[1], account_key: "@EU3"}
  ])

Enchant.create!([
  {name: "Magicka",                 description: "Adds %1 Max Magicka"},
  {name: "Health",                  description: "Adds %1 Max Health"},
  {name: "Stamina",                 description: "Adds %1 Max Stamina"},
  {name: "Frost",                   description: "Deals %1 Frost damage"},
  {name: "Foulness",                description: "Deals %1 Disease damage"},
  {name: "Poison",                  description: "Deals %1 Poison damage"},
  {name: "Shock",                   description: "Deals %1 Shock damage"},
  {name: "Flame",                   description: "Deals %1 Flame damage"},
  {name: "Decrease Health",         description: "Deals %1 unresistable damage"},
  {name: "Rage",                    description: "Increases your power by %1 for %2 seconds"},
  {name: "Hardening",               description: "Grants a %1 point Damage Shield for %2 seconds"},
  {name: "Absorb Magicka",          description: "Deals %1 Magic damage and recovers %2 Magicka"},
  {name: "Absorb Health",           description: "Deals %1 Magic damage and recovers %2 Health"},
  {name: "Absorb Stamina",          description: "Deals %1 Magic damage and recovers %2 Stamina"},
  {name: "Weakening",               description: "Reduce target’s power by %1 for %2 seconds"},
  {name: "Crushing",                description: "Reduce target’s armor by %1 for %2 seconds"},
  {name: "Health Regen",            description: "Adds %1 Health Recovery"},
  {name: "Magicka Regen",           description: "Adds %1 Magicka Recovery"},
  {name: "Stamina Regen",           description: "Adds %1 Stamina Recovery"},
  {name: "Bashing",                 description: "Increase bash damage by %1"},
  {name: "Increase Magical Harm",   description: "Adds %1 Spell Damage"},
  {name: "Increase Physical Harm",  description: "Adds %1 Weapon Damage"},
  {name: "Decrease Spell Harm",     description: "Adds %1 Spell Resistance"},
  {name: "Decrease Physical Harm",  description: "Adds %1 Armor"},
  {name: "Frost Resist",            description: "Adds %1 Frost Resistance"},
  {name: "Poison Resist",           description: "Adds %1 Poison Resistance"},
  {name: "Disease Resist",          description: "Adds %1 Disease Resistance"},
  {name: "Fire Resist",             description: "Adds %1 Fire Resistance"},
  {name: "Shock Resist",            description: "Adds %1 Shock Resistance"},
  {name: "Shielding",               description: "Reduce cost of Bash and reduce cost of Blocking"},
  {name: "Reduce Feat Cost",        description: "Reduce stamina cost of abilities by %1"},
  {name: "Reduce Spell Cost",       description: "Reduce magicka cost of spells by %1"},
  {name: "Potion Boost",            description: "Increase Potion effect by %1"},
  {name: "Potion Speed",            description: "Reduce the cooldown of Potions below this item’s level by %1 seconds"}
  ])

SetBonusTrait.create!([
  # Crafted
  {name: "Night's Silence", description: ""},
  {name: "Ashen Grip", description: ""},
  {name: "Death's Wind", description: ""},
  {name: "Torug's Pact", description: ""},
  {name: "Seducer", description: ""},
  {name: "Twilight's Embrace", description: ""},
  {name: "Whitestrake's Retribution", description: ""},
  {name: "Hist Bark", description: ""},
  {name: "Magnus' Gift", description: ""},
  {name: "Alessia's Bulwark", description: ""},
  {name: "Song of Lamae", description: ""},
  {name: "Vampire's Kiss", description: ""},
  {name: "Hunding's Rage", description: ""},
  {name: "Willow's Path", description: ""},
  {name: "Night Mother's Gaze", description: ""},
  {name: "Spectre's Eye", description: ""},
  {name: "Eyes of Mara", description: ""},
  {name: "Kagrenac's Hope", description: ""},
  {name: "Shalidor's Curse", description: ""},
  {name: "The Oblivion's Foe", description: ""},
  {name: "Orgnum's Scales", description: ""},

  # PvP
  {name: "Curse Eater", description: ""},
  {name: "Ravager", description: ""},
  {name: "Twin Sisters", description: ""},
  {name: "Grace of the Ancients", description: ""},
  {name: "Cyrodiil's Light", description: ""},
  {name: "Crest of Cyrodiil", description: ""},
  {name: "Ward of Cyrodiil", description: ""},
  {name: "Buffer of the Swift", description: ""},
  {name: "Shield of the Valiant", description: ""},
  {name: "Bastion of the Heartland", description: ""},
  {name: "Blessing of the Potentates", description: ""},
  {name: "Deadly Strike", description: ""},
  {name: "Eagle Eye", description: ""},
  {name: "Vengeance Leech", description: ""},
  {name: "Wrath of the Imperium", description: ""},

  # PvE
  {name: "Adroitness", description: ""},
  {name: "Akaviri Dragonguard", description: ""},
  {name: "Akatosh's Blessed Armor", description: ""},
  {name: "Allessian Order", description: ""},
  {name: "Armor of the Construct", description: ""},
  {name: "Armor of Rage", description: ""},
  {name: "Arms of the Ancestors", description: ""},
  {name: "Arms of Infernace", description: ""},
  {name: "Brute", description: ""},
  {name: "Coat of the Red Mountain", description: ""},
  {name: "Darkstride", description: ""},
  {name: "Destruction Suit", description: ""},
  {name: "Draugr's Heritage", description: ""},
  {name: "Dreugh King Slayer", description: ""},
  {name: "Ebon Armory", description: ""},
  {name: "Elegance", description: ""},
  {name: "Fiord's Legacy", description: ""},
  {name: "Healer's Habit", description: ""},
  {name: "Hide of the Werewolf", description: ""},
  {name: "Hircine's Veneer", description: ""},
  {name: "Ice Furnace", description: ""},
  {name: "Indarys Ensemble", description: ""},
  {name: "Juggernaut", description: ""},
  {name: "Kyne's Flight", description: ""},
  {name: "Knightmare", description: ""},
  {name: "Lord's Mail", description: ""},
  {name: "Magicka Furnace", description: ""},
  {name: "Morage Tong", description: ""},
  {name: "Necropotence", description: ""},
  {name: "Night Mother's Embrace", description: ""},
  {name: "Nightshade", description: ""},
  {name: "Nikulas' Heavy Armor", description: ""},
  {name: "Oblivion's Edge", description: ""},
  {name: "Prisoner's Rags", description: ""},
  {name: "Ranger's Gait", description: ""},
  {name: "Relics of the Rebellion", description: ""},
  {name: "Robes of the Necromancer", description: ""},
  {name: "Savior's Hide", description: ""},
  {name: "Sanctuary", description: ""},
  {name: "Shadow Dancer's Raiment", description: ""},
  {name: "Shrouded Armor", description: ""},
  {name: "Shroud of the Lich", description: ""},
  {name: "Skirmisher's Bite", description: ""},
  {name: "Silks of the Sun", description: ""},
  {name: "Soulshine", description: ""},
  {name: "Stendarr's Embrace", description: ""},
  {name: "Storm Knight's Plate", description: ""},
  {name: "Stygian", description: ""},
  {name: "Syrabane's Grip", description: ""},
  {name: "Thunderous Plate Mail", description: ""},
  {name: "Treasure's of the Earthforge", description: ""},
  {name: "Unassailable", description: ""},
  {name: "Vestments of the Warlock", description: ""},
  {name: "Viper's Sting", description: ""},
  {name: "Worm's Raiment", description: ""}
  ])

ResearchableTrait.create!([
  {name: "Powered", description: "Reduces the cooldown of weapon enchants"},
  {name: "Charged", description: "Increases enchantment charges"},
  {name: "Precise", description: "Increase Weapon and Spell Critical values"},
  {name: "Infused", description: "Increase weapon enchantment effect"},
  {name: "Defending", description: "Increases total Armor and Spell Resistance"},
  {name: "Training", description: "Increase weapon skill line experience with this weapon type"},
  {name: "Sharpened", description: "Increase Armor and Spell Penetration"},
  {name: "Weighted", description: "Increase weapon attack speed"},
  {name: "Sturdy", description: "Chance to avoid decay when defeated"},
  {name: "Impenetrable", description: "Increase resistance to Critical hits"},
  {name: "Reinforced", description: "Increases this items Armor value"},
  {name: "Training", description: "Increase armor skill line experience with this armor type"},
  {name: "Well-fitted", description: "Reduces the cost of Sprinting"},
  {name: "Infused", description: "Increase armor enchantment effect"},
  {name: "Exploration", description: "Increase exploration experience gained"},
  {name: "Divines", description: "Increases Mundus Stone effects"}
  ])

Rarity.create!([
  {name: "Normal"},
  {name: "Fine"},
  {name: "Superior"},
  {name: "Epic"},
  {name: "Legendary"}
  ])

Style.create!([
  {name: "Breton"},
  {name: "Redguard"},
  {name: "Orc"},
  {name: "Dunmer"},
  {name: "Nord"},
  {name: "Argonian"},
  {name: "Altmer"},
  {name: "Bosmer"},
  {name: "Khajiit"},
  {name: "Ancient Elf"},
  {name: "Barbaric"},
  {name: "Primal"},
  {name: "Daedric"},
  {name: "Imperial"}
  ])

item_type_definitions = [
  {
    name: "Weapon",
    subs: [
      {name: "Bow"},
      {name: "Staff (Inferno)"},
      {name: "Staff (Ice)"},
      {name: "Staff (Lightning)"},
      {name: "Staff (Restoration)"},
      {name: "Axe"},
      {name: "Hammer"},
      {name: "Sword"},
      {name: "Battle Axe"},
      {name: "Maul"},
      {name: "Greatsword"},
      {name: "Dagger"}
    ]
  },
  {
    name: "Apparel",
    subs: [
      {name: "Shield"}
    ]
  },
  {
    name: "Apparel (Heavy)",
    subs: [
      {name: "Chest"},
      {name: "Feet"},
      {name: "Hand"},
      {name: "Head"},
      {name: "Legs"},
      {name: "Shoulders"},
      {name: "Waist"}
    ]
  },
  {
    name: "Apparel (Medium)",
    subs: [
      {name: "Chest"},
      {name: "Feet"},
      {name: "Hand"},
      {name: "Head"},
      {name: "Legs"},
      {name: "Shoulders"},
      {name: "Waist"}
    ]
  },
  {
    name: "Apparel (Light)",
    subs: [
      {name: "Chest"},
      {name: "Feet"},
      {name: "Hand"},
      {name: "Head"},
      {name: "Legs"},
      {name: "Shoulders"},
      {name: "Waist"}
    ]
  },
  {
    name: "Accessory",
    subs: [
      {name: "Ring"},
      {name: "Neck"},
    ]
  },
  {
    name: "Material",
    subs: [
      {name: "Woodworking"},
      {name: "Woodworking (Raw)"},
      {name: "Woodworking (Tannin)"},
      {name: "Blacksmithing"},
      {name: "Blacksmithing (Raw)"},
      {name: "Blacksmithing (Temper)"},
      {name: "Clothing"},
      {name: "Clothing (Raw)"},
      {name: "Clothing (Resin)"},
      {name: "Alchemy (Solvent)"},
      {name: "Alchemy (Reagent)"},
      {name: "Provisioning (Flavoring)"},
      {name: "Provisioning (Spice)"},
      {name: "Enchanting (Runestone)"},
      {name: "Style"}
    ]
  },
  {
    name: "Consumable",
    subs: [
      {name: "Recipe"},
      {name: "Potion"},
      {name: "Lure"}
    ]
  }
]

item_type_definitions.each do |type_definition|
  type = ItemType.create!(name: type_definition[:name])

  type_definition[:subs].each do |subtype_definition|
    ItemSubtype.create!(item_type: type, name: subtype_definition[:name])
  end
end

ItemListingType.create!([
  {name: "WTB"},
  {name: "WTS"},
  {name: "WTT"},
  ])

ItemListingStatus.create!([
  {name: "Listed"},
  {name: "Has Bids"},
  {name: "Bought"},
  {name: "Has Offer"},
  {name: "Offer Accepted"},
  {name: "Delivered"},
  {name: "Received"},
  {name: "Cancelled"},
  {name: "Expired"}
  ])

CancellationReason.create!([
  {reason: "Sold item on another site"},
  {reason: "Sold item in game"},
  {reason: "No longer have item"},
  {reason: "No longer wish to sell item"}
  ])

item_definitions = [
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Blacksmithing (Raw)"),
    items: [
      {name: "Iron Ore", level: 1},
      {name: "Steel Ore", level: 1},
      {name: "Orichalcum Ore", level: 1},
      {name: "Dwarven Ore", level: 1},
      {name: "Ebony Ore", level: 1},
      {name: "Calcinium Ore", level: 1},
      {name: "Galatite Ore", level: 1},
      {name: "Quicksilver Ore", level: 1},
      {name: "Voidstone Ore", level: 1}
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Blacksmithing"),
    items: [
      {name: "Iron Ingot", level: 1},
      {name: "Steel Ingot", level: 1},
      {name: "Orichalcum Ingot", level: 1},
      {name: "Dwarven Ingot", level: 1},
      {name: "Ebony Ingot", level: 1},
      {name: "Calcinium Ingot", level: 1},
      {name: "Galatite Ingot", level: 1},
      {name: "Quicksilver Ingot", level: 1},
      {name: "Voidstone Ingot", level: 1}
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Blacksmithing (Temper)"),
    items: [
      {name: "Honing Stone", level: 1, rarity: Rarity.find_by_name("Fine")},
      {name: "Dwarven Oil", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Grain Solvent", level: 1, rarity: Rarity.find_by_name("Epic")},
      {name: "Tempering Alloy", level: 1, rarity: Rarity.find_by_name("Legendary")},
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Woodworking (Raw)"),
    items: [
      {name: "Raw Maple", level: 1},
      {name: "Raw Oak", level: 1},
      {name: "Raw Beech", level: 1},
      {name: "Raw Hickory", level: 1},
      {name: "Raw Yew", level: 1},
      {name: "Raw Birch", level: 1},
      {name: "Raw Ash", level: 1},
      {name: "Raw Mahogany", level: 1},
      {name: "Raw Nightwood", level: 1},
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Woodworking"),
    items: [
      {name: "Sanded Maple", level: 1},
      {name: "Sanded Oak", level: 1},
      {name: "Sanded Beech", level: 1},
      {name: "Sanded Hickory", level: 1},
      {name: "Sanded Yew", level: 1},
      {name: "Sanded Birch", level: 1},
      {name: "Sanded Ash", level: 1},
      {name: "Sanded Mahogany", level: 1},
      {name: "Sanded Nightwood", level: 1},
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Woodworking (Tannin)"),
    items: [
      {name: "Pitch", level: 1, rarity: Rarity.find_by_name("Fine")},
      {name: "Turpen", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Mastic", level: 1, rarity: Rarity.find_by_name("Epic")},
      {name: "Rosin", level: 1, rarity: Rarity.find_by_name("Legendary")},
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Clothing (Raw)"),
    items: [
      {name: "Raw Jute", level: 1},
      {name: "Raw Flax", level: 1},
      {name: "Raw Cotton", level: 1},
      {name: "Raw Spidersilk", level: 1},
      {name: "Raw Ebonthread", level: 1},
      {name: "Raw Kresh Fiber", level: 1},
      {name: "Raw Ironthread", level: 1},
      {name: "Raw Silverweave", level: 1},
      {name: "Raw Void Cloth", level: 1},
      {name: "Raw Rawhide", level: 1},
      {name: "Raw Hide", level: 1},
      {name: "Raw Leather", level: 1},
      {name: "Raw Thick Leather", level: 1},
      {name: "Raw Fell Hide", level: 1},
      {name: "Raw Topgrain Hide", level: 1},
      {name: "Raw Iron Hide", level: 1},
      {name: "Raw Superb Hide", level: 1},
      {name: "Raw Shadowhide", level: 1}
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Clothing"),
    items: [
      {name: "Jute", level: 1},
      {name: "Flax", level: 1},
      {name: "Cotton", level: 1},
      {name: "Spidersilk", level: 1},
      {name: "Ebonthread", level: 1},
      {name: "Kresh Fiber", level: 1},
      {name: "Ironthread", level: 1},
      {name: "Silverweave", level: 1},
      {name: "Void Cloth", level: 1},
      {name: "Rawhide", level: 1},
      {name: "Hide", level: 1},
      {name: "Leather", level: 1},
      {name: "Thick Leather", level: 1},
      {name: "Fell Hide", level: 1},
      {name: "Topgrain Hide", level: 1},
      {name: "Iron Hide", level: 1},
      {name: "Superb Hide", level: 1},
      {name: "Shadowhide", level: 1}
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Clothing (Resin)"),
    items: [
      {name: "Hemming", level: 1, rarity: Rarity.find_by_name("Fine")},
      {name: "Embroidery", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Elegant Lining", level: 1, rarity: Rarity.find_by_name("Epic")},
      {name: "Dreugh Wax", level: 1, rarity: Rarity.find_by_name("Legendary")},
    ]
  },
  {
    item_type: ItemType.find_by_name("Consumable"),
    item_subtype: ItemSubtype.find_by_name("Recipe"),
    items: [
      {name: "Racial Motifs 1: The High Elves", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 2: The Dark Elves", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 3: The Wood Elves", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 4: The Nords", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 5: The Bretons", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 6: The Redguards", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 7: The Khajiit", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 8: The Orcs", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 9: The Argonians", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 10: The Imperials", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 11: Ancient Elves", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 12: Barbaric", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 13: Primal", level: 1, rarity: Rarity.find_by_name("Superior")},
      {name: "Racial Motifs 14: Daedric", level: 1, rarity: Rarity.find_by_name("Superior")}
    ]
  },
  {
    item_type: ItemType.find_by_name("Material"),
    item_subtype: ItemSubtype.find_by_name("Enchanting (Runestone)"),
    items: [
      {name: "Dekeipa", level: 1},
      {name: "Taderi", level: 1},
      {name: "Ta", level: 1},
      {name: "Rakeipa", level: 1},
      {name: "Oru", level: 1},
      {name: "Okori", level: 1},
      {name: "Okoma", level: 1},
      {name: "Oko", level: 1},
      {name: "Meip", level: 1},
      {name: "Makkoma", level: 1},
      {name: "Makko", level: 1},
      {name: "Makderi", level: 1},
      {name: "Lire", level: 1},
      {name: "Kuoko", level: 1},
      {name: "Kaderi", level: 1},
      {name: "Jaedi", level: 1},
      {name: "Haoko", level: 1},
      {name: "Deteri", level: 1},
      {name: "Denima", level: 1},
      {name: "Deni", level: 1},
      {name: "Jora", level: 5},
      {name: "Jode", level: 5},
      {name: "Jejota", level: 6},
      {name: "Porade", level: 10},
      {name: "Notade", level: 10},
      {name: "Denata", level: 11},
      {name: "Ode", level: 15},
      {name: "Jera", level: 15},
      {name: "Rekuta", level: 16},
      {name: "Tade", level: 20},
      {name: "Jejora", level: 20},
      {name: "Kuta", level: 21},
      {name: "Odra", level: 25},
      {name: "Jayde", level: 25},
      {name: "Pojora", level: 30},
      {name: "Edode", level: 30},
      {name: "Pojode", level: 35},
      {name: "Edora", level: 35},
      {name: "Rekude", level: 40},
      {name: "Jaera", level: 40},
      {name: "Pora", level: 45},
      {name: "Hade", level: 45},
      {name: "Cura", level: 50},
      {name: "Rera", level: 50},
      {name: "Rede", level: 50},
      {name: "Recura", level: 50},
      {name: "Pode", level: 50},
      {name: "Kude", level: 50},
      {name: "Kedeko", level: 50},
      {name: "Idode", level: 50},
      {name: "Derado", level: 50},
      {name: "Denara", level: 50}
    ]
  }
]

common_quality = Rarity.find_by_name("Common")

item_definitions.each do |item_definition|
  item_hashes = []

  item_definition[:items].each do |item_hash|
    item_hash[:item_type] = item_definition[:item_type]
    item_hash[:item_subtype] = item_definition[:item_subtype]
    item_hash[:user] = admin

    unless item_hash[:rarity].present?
      item_hash[:rarity] = common_quality
    end

    item_hashes << item_hash
  end

  Item.create!(item_hashes)
end

if Rails.env == 'development'
  listings = []

  1000.times do
    user = User.order("RANDOM()").first
    account = user.accounts.order("RANDOM()").first
    bid = 10 + rand(500)

    listings << {
      user: user,
      account: account,
      item: Item.order("RANDOM()").first,
      starting_bid: bid,
      current_bid: bid,
      buyout: 50 + rand(700),
      expiration_date: 7.days.from_now,
      item_listing_type: ItemListingType.order("RANDOM()").first,
      item_listing_status: ItemListingStatus.find_by_name("Listed") #ItemListingStatus.order("RANDOM()").first
    }
  end

  ItemListing.create!(listings)
end

#==============================================================================
# Template for new PROD Seed Data
#
# => Step 1: Comment out EVERYTHING above this section
# => Step 2: Copy the below and paste it below this commented section and
#             insert any new item types or items into the appropriate sections
#             or delete them if not needed
# => Step 3: Add any other seed data needed
# => Step 4: Save, commit, and push to heroku
# => Step 5: Run this command -> heroku run rake db:seed
# => Step 6: Make sure all of the new data you added makes its way into the
#             development/test seed section above
# => Step 7: Delete your temporary prod seed section and then
#             save, commit, and push to bitbucket
#==============================================================================

# admin = User.find(1)

# item_type_definitions = [
#   {
#     name: "",
#     subs: [
#       {name: ""},
#     ]
#   }
# ]

# item_type_definitions.each do |type_definition|
#   type = ItemType.create!(name: type_definition[:name])

#   type_definition[:subs].each do |subtype_definition|
#     ItemSubtype.create!(item_type: type, name: subtype_definition[:name])
#   end
# end

# item_definitions = [
#   {
#     item_type: ItemType.find_by_name(""),
#     item_subtype: ItemSubtype.find_by_name(""),
#     items: [
#       {name: "", level: 1, rarity: Rarity.find_by_name("Fine")},
#       {name: "", level: 1, rarity: Rarity.find_by_name("Superior")},
#       {name: "", level: 1, rarity: Rarity.find_by_name("Epic")},
#       {name: "", level: 1, rarity: Rarity.find_by_name("Legendary")},
#     ]
#   },
# ]

# common_quality = Rarity.find_by_name("Common")

# item_definitions.each do |item_definition|
#   item_hashes = []

#   item_definition[:items].each do |item_hash|
#     item_hash[:item_type] = item_definition[:item_type]
#     item_hash[:item_subtype] = item_definition[:item_subtype]
#     item_hash[:user] = admin

#     unless item_hash[:rarity].present?
#       item_hash[:rarity] = common_quality
#     end

#     item_hashes << item_hash
#   end

#   Item.create!(item_hashes)
# end
