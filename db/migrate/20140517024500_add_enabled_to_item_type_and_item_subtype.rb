class AddEnabledToItemTypeAndItemSubtype < ActiveRecord::Migration
  def change
    add_column :item_types, :enabled, :boolean, null: false, default: true
    add_column :item_subtypes, :enabled, :boolean, null: false, default: true
  end
end
