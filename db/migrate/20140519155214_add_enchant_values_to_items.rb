class AddEnchantValuesToItems < ActiveRecord::Migration
  def change
    add_column :items, :enchant_1_value, :integer
    add_column :items, :enchant_1_value_2, :integer

    add_column :items, :enchant_2_value, :integer
    add_column :items, :enchant_2_value_2, :integer
  end
end
