class CreateCancellationReasons < ActiveRecord::Migration
  def change
    create_table :cancellation_reasons do |t|
      t.text :reason

      t.timestamps
    end
  end
end
