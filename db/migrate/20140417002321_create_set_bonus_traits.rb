class CreateSetBonusTraits < ActiveRecord::Migration
  def change
    create_table :set_bonus_traits do |t|
      t.string :name, null: false
      t.string :description

      t.timestamps
    end
  end
end
