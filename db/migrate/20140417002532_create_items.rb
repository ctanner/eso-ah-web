class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name, null: false

      t.references :item_type, null: false, index: true
      t.references :item_subtype, null: false, index: true

      t.integer :level, index: true
      t.integer :veteran_level, index: true

      t.references :rarity, index: true
      t.references :style, index: true

      t.references :set_bonus_trait, index: true
      t.references :researchable_trait, index: true

      t.integer :enchant_1_id
      t.integer :enchant_2_id

      t.references :user, null: false, index: true

      t.timestamps
    end
  end
end
