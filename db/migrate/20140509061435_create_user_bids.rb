class CreateUserBids < ActiveRecord::Migration
  def change
    create_table :user_bids do |t|
      t.references :user, null: false, index: true
      t.references :account, null: false, index: true
      t.references :item_listing, null: false, index: true
      t.integer :quantity
      t.integer :bid_amount

      t.timestamps
    end
  end
end
