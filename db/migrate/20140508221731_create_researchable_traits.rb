class CreateResearchableTraits < ActiveRecord::Migration
  def change
    create_table :researchable_traits do |t|
      t.string :name, null: false
      t.string :description

      t.timestamps
    end
  end
end
