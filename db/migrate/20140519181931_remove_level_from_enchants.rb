class RemoveLevelFromEnchants < ActiveRecord::Migration
  def change
    remove_column :enchants, :level, :integer
  end
end
