class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :user, null: false, index: true
      t.references :server, null: false
      t.string :account_key, null: false

      t.boolean :validated, default: false

      t.timestamps
    end
  end
end
