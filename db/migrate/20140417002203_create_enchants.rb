class CreateEnchants < ActiveRecord::Migration
  def change
    create_table :enchants do |t|
      t.string :name, null: false
      t.string :description
      t.integer :level

      t.timestamps
    end
  end
end
