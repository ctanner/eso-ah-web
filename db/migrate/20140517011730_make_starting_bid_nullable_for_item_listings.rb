class MakeStartingBidNullableForItemListings < ActiveRecord::Migration
  def up
    execute "ALTER TABLE item_listings ALTER COLUMN starting_bid DROP DEFAULT;"
  end

  def down
    execute "ALTER TABLE item_listings ALTER COLUMN starting_bid SET DEFAULT 1;"
  end
end
