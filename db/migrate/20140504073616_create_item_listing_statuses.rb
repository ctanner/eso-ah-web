class CreateItemListingStatuses < ActiveRecord::Migration
  def change
    create_table :item_listing_statuses do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
