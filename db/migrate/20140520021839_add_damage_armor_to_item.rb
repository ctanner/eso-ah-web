class AddDamageArmorToItem < ActiveRecord::Migration
  def change
    add_column :items, :damage, :integer
    add_column :items, :armor, :integer
  end
end
