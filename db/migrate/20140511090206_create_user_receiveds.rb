class CreateUserReceiveds < ActiveRecord::Migration
  def change
    create_table :user_receiveds do |t|
      t.references :user, null: false, index: true
      t.references :account, null: false, index: true
      t.references :item_listing, null: false, index: true

      t.timestamps
    end
  end
end
