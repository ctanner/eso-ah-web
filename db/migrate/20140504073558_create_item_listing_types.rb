class CreateItemListingTypes < ActiveRecord::Migration
  def change
    create_table :item_listing_types do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
