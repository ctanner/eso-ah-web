class CreateItemSubtypes < ActiveRecord::Migration
  def change
    create_table :item_subtypes do |t|
      t.string :name, null: false
      t.references :item_type, null: false, index: true

      t.timestamps
    end
  end
end
