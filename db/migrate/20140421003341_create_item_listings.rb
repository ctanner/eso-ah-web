class CreateItemListings < ActiveRecord::Migration
  def change
    create_table :item_listings do |t|
      t.references :user, null: false, index: true
      t.references :account, null: false, index: true
      t.references :item, null: false, index: true
      t.integer :quantity, default: 1
      t.integer :starting_bid, default: 1
      t.integer :bid_increment, default: 1
      t.integer :current_bid
      t.integer :buyout
      t.date :expiration_date

      t.references :item_listing_type, null: false, index: true
      t.references :item_listing_status, null: false, default: 0, index: true
      t.references :cancellation_reason

      t.integer :high_bidder_user_id
      t.integer :high_bidder_account_id

      t.integer :consumer_user_id     # user_id of buyer or seller
      t.integer :consumer_account_id  # account_id of buyer or seller

      t.timestamps
    end
  end
end
