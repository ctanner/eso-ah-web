Rails.application.routes.draw do
  root to: 'home#index'

  get 'about' => 'home#about', as: 'home_about'
  get 'contact' => 'home#contact', as: 'home_contact'
  get 'privacy' => 'home#privacy', as: 'home_privacy'
  get 'site_policies' => 'home#site_policies', as: 'home_site_policies'
  get 'news' => 'home#news', as: 'home_news'
  get 'changes' => 'home#changes', as: 'home_changes'

  get 'contest' => 'home#contest', as: 'home_contest'
  get 'contest_rules' => 'home#contest_rules', as: 'home_contest_rules'
  get 'contest_prizes' => 'home#contest_prizes', as: 'home_contest_prizes'
  get 'sweeps' => 'home#sweeps', as: 'home_sweeps'
  get 'sweeps_rules' => 'home#sweeps_rules', as: 'home_sweeps_rules'

  devise_for :users, controllers: { registrations: 'users/registrations' }

  scope '/users/:user_id', module: 'users' do
    get 'profile' => 'profile#show', as: 'user_profile'

    resources :accounts, as: 'user_accounts' do
      collection do
        get :select
      end
    end

    resources :item_listings, as: 'user_item_listings' do
      collection do
        get :sales
        get :purchases
        get :offers
        post :cancel
      end

      member do
        get :delivered
        get :received
      end
    end
  end

  get 'items/update_subtypes', as: 'items_update_subtypes'
  get 'items/update_form', as: 'items_update_form'

  resources :items do
    collection do
      get :autocomplete
    end
  end

  resources :item_listings, only: [:index, :show] do
    collection do
      get :wtb
      get :wts
      get :wtt
    end

    member do
      get :bid
      get :buy
      get :offer
    end
  end

=begin
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      namespace :items do
        resources :traits
        resources :tiers
        resources :styles
        resources :set_bonuses
        resources :rarities
        resources :enchants
        resources :configurations
      end

      resources :games
      resources :items
      resources :item_listings
    end
  end
=end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
