module ItemListingsHelper
  def expires_in(item_listing)
    exp = item_listing.expiration_date

    if exp > 24.hours.from_now
      return "Days"
    elsif exp > 12.hours.from_now
      return "Long"
    elsif exp > 6.hours.from_now
      return "Medium"
    else #if exp > 1.hour.from_now
      return "Short"
    end
  end

  def item_listing_durations
    if current_user.premium?
      [
        ItemListingDuration.new("6 hours", 0.25.days.from_now),
        ItemListingDuration.new("12 hours", 0.5.days.from_now),
        ItemListingDuration.new("1 day", 1.day.from_now),
        ItemListingDuration.new("3 days", 3.days.from_now),
        ItemListingDuration.new("7 days", 7.days.from_now)
      ]
    else
      [
        ItemListingDuration.new("3 days", 3.days.from_now),
        ItemListingDuration.new("7 days", 7.days.from_now)
      ]
    end
  end
end
