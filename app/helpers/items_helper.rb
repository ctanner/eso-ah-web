module ItemsHelper
  def level_display(item)
    if item.level.nil?
      return "V Rank #{item.veteran_level}" unless item.veteran_level.nil?
    else
      return "Level #{item.level.to_s}"
    end
  end

  def enchant_tooltip_display(description, value1, value2)
    description.gsub("%1", value1.to_s).gsub("%2", value2.to_s)
  end
end
