var fade_flash = function() {
  $('#flash-messages .alert').delay(5000).fadeOut('slow', function() {
    $(this).empty();
  });
};

fade_flash();

$(document).ajaxComplete(function(event, request) {
  var msg = request.getResponseHeader('X-Message');
  var type = request.getResponseHeader('X-Message-Type');

  if(msg && type)
  {
    var msg_class = 'alert-success';

    if(type === 'error')
      msg_class = 'alert-danger';
    else if(type === 'warning')
      msg_class = 'alert-warning';

    $('#flash-messages').html('<div class="alert fade in ' + msg_class + '" data-alert="alert"><p>' + msg + '</p></div>');
    fade_flash();
  }
});
