$(document).on('ready page:change', function() {
  enchantChanged('.item-form-enchant1');
  enchantChanged('.item-form-enchant2');

  $('.item-form-type').off('change').on('change', function() {
    $.ajax({
      url: items_update_subtypes_path(),
      dataType: "script",
      data: {
        "type_id": $('.item-form-type').val()
      }
    });
  });

  $('.item-form-level').off('change').on('change', function() {
    if($(this).val())
    {
      $('.item-form-veteran-level').val(null);
    }
  });

  $('.item-form-veteran-level').off('change').on('change', function() {
    if($(this).val())
    {
      $('.item-form-level').val(null);
    }
  });

  $('#item-form .item-form-items').off('change').on('change', function() {
    $.ajax({
      url: items_update_form_path(),
      dataType: "script",
      data: {
        "item_id": $('#item-form .item-form-items').val()
      }
    });
  });

  $('.item-form-enchant1 .enchant-select').off('change').on('change', function() {
    enchantChanged('.item-form-enchant1');
  });

  $('.item-form-enchant2 .enchant-select').off('change').on('change', function() {
    enchantChanged('.item-form-enchant2');
  });
});

function enchantChanged(enchantGroup) {
  var group = $(enchantGroup);
  var selected = group.find('.enchant-select option:selected').text();
  var value2 = group.find('.enchant-value-2');

  if(_.contains([
      "Rage",
      "Hardening",
      "Absorb Magicka",
      "Absorb Health",
      "Absorb Stamina",
      "Weakening",
      "Crushing"
    ], selected)) {
    value2.closest('.form-group').show();
  } else {
    value2.closest('.form-group').hide();
  }
}
