
  function root_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/'
    }else {
      var params = options;
      return '/'
    }
  }

  function home_about_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/about?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/about'
    }else {
      var params = options;
      return '/about'
    }
  }

  function home_contact_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/contact?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/contact'
    }else {
      var params = options;
      return '/contact'
    }
  }

  function home_privacy_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/privacy?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/privacy'
    }else {
      var params = options;
      return '/privacy'
    }
  }

  function home_contest_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/contest?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/contest'
    }else {
      var params = options;
      return '/contest'
    }
  }

  function home_contest_rules_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/contest_rules?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/contest_rules'
    }else {
      var params = options;
      return '/contest_rules'
    }
  }

  function home_sweeps_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/sweeps?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/sweeps'
    }else {
      var params = options;
      return '/sweeps'
    }
  }

  function home_sweeps_rules_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/sweeps_rules?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/sweeps_rules'
    }else {
      var params = options;
      return '/sweeps_rules'
    }
  }

  function new_user_session_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/sign_in?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/sign_in'
    }else {
      var params = options;
      return '/users/sign_in'
    }
  }

  function user_session_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/sign_in?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/sign_in'
    }else {
      var params = options;
      return '/users/sign_in'
    }
  }

  function destroy_user_session_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/sign_out?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/sign_out'
    }else {
      var params = options;
      return '/users/sign_out'
    }
  }

  function user_password_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/password?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/password'
    }else {
      var params = options;
      return '/users/password'
    }
  }

  function new_user_password_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/password/new?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/password/new'
    }else {
      var params = options;
      return '/users/password/new'
    }
  }

  function edit_user_password_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/password/edit?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/password/edit'
    }else {
      var params = options;
      return '/users/password/edit'
    }
  }

  function cancel_user_registration_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/cancel?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/cancel'
    }else {
      var params = options;
      return '/users/cancel'
    }
  }

  function user_registration_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users'
    }else {
      var params = options;
      return '/users'
    }
  }

  function new_user_registration_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/sign_up?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/sign_up'
    }else {
      var params = options;
      return '/users/sign_up'
    }
  }

  function edit_user_registration_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/edit?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/edit'
    }else {
      var params = options;
      return '/users/edit'
    }
  }

  function user_confirmation_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/confirmation?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/confirmation'
    }else {
      var params = options;
      return '/users/confirmation'
    }
  }

  function new_user_confirmation_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/confirmation/new?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/confirmation/new'
    }else {
      var params = options;
      return '/users/confirmation/new'
    }
  }

  function user_profile_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/profile?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/profile'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/profile'
    }
  }

  function select_user_accounts_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/select?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/select'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/accounts/select'
    }
  }

  function user_accounts_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/accounts?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/accounts'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/accounts'
    }
  }

  function new_user_account_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/new?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/new'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/accounts/new'
    }
  }

  function edit_user_account_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/' + params.id + '/edit?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/' + params.id + '/edit'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/accounts/' + params.id + '/edit'
    }
  }

  function user_account_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/' + params.id + '?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/accounts/' + params.id + ''
    }else {
      var params = options;
      return '/users/' + params.user_id + '/accounts/' + params.id + ''
    }
  }

  function sales_user_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/sales?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/sales'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/sales'
    }
  }

  function purchases_user_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/purchases?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/purchases'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/purchases'
    }
  }

  function offers_user_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/offers?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/offers'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/offers'
    }
  }

  function cancel_user_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/cancel?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/cancel'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/cancel'
    }
  }

  function update_items_form_user_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/update_items_form?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/update_items_form'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/update_items_form'
    }
  }

  function delivered_user_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/delivered?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/delivered'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/delivered'
    }
  }

  function received_user_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/received?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/received'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/received'
    }
  }

  function user_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings'
    }
  }

  function new_user_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/new?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/new'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/new'
    }
  }

  function edit_user_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/edit?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/edit'
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '/edit'
    }
  }

  function user_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + '?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/users/' + params.user_id + '/item_listings/' + params.id + ''
    }else {
      var params = options;
      return '/users/' + params.user_id + '/item_listings/' + params.id + ''
    }
  }

  function items_update_subtypes_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/items/update_subtypes?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/items/update_subtypes'
    }else {
      var params = options;
      return '/items/update_subtypes'
    }
  }

  function items_update_form_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/items/update_form?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/items/update_form'
    }else {
      var params = options;
      return '/items/update_form'
    }
  }

  function autocomplete_items_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/items/autocomplete?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/items/autocomplete'
    }else {
      var params = options;
      return '/items/autocomplete'
    }
  }

  function items_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/items?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/items'
    }else {
      var params = options;
      return '/items'
    }
  }

  function new_item_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/items/new?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/items/new'
    }else {
      var params = options;
      return '/items/new'
    }
  }

  function edit_item_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/items/' + params.id + '/edit?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/items/' + params.id + '/edit'
    }else {
      var params = options;
      return '/items/' + params.id + '/edit'
    }
  }

  function item_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/items/' + params.id + '?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/items/' + params.id + ''
    }else {
      var params = options;
      return '/items/' + params.id + ''
    }
  }

  function wtb_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings/wtb?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings/wtb'
    }else {
      var params = options;
      return '/item_listings/wtb'
    }
  }

  function wts_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings/wts?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings/wts'
    }else {
      var params = options;
      return '/item_listings/wts'
    }
  }

  function wtt_item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings/wtt?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings/wtt'
    }else {
      var params = options;
      return '/item_listings/wtt'
    }
  }

  function bid_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings/' + params.id + '/bid?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings/' + params.id + '/bid'
    }else {
      var params = options;
      return '/item_listings/' + params.id + '/bid'
    }
  }

  function buy_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings/' + params.id + '/buy?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings/' + params.id + '/buy'
    }else {
      var params = options;
      return '/item_listings/' + params.id + '/buy'
    }
  }

  function offer_item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings/' + params.id + '/offer?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings/' + params.id + '/offer'
    }else {
      var params = options;
      return '/item_listings/' + params.id + '/offer'
    }
  }

  function item_listings_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings'
    }else {
      var params = options;
      return '/item_listings'
    }
  }

  function item_listing_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/item_listings/' + params.id + '?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/item_listings/' + params.id + ''
    }else {
      var params = options;
      return '/item_listings/' + params.id + ''
    }
  }

  function rails_mailers_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/rails/mailers?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/rails/mailers'
    }else {
      var params = options;
      return '/rails/mailers'
    }
  }

  function rails_info_properties_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/rails/info/properties?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/rails/info/properties'
    }else {
      var params = options;
      return '/rails/info/properties'
    }
  }

  function rails_info_routes_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/rails/info/routes?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/rails/info/routes'
    }else {
      var params = options;
      return '/rails/info/routes'
    }
  }

  function rails_info_path(options){
    if(options && options.data) {
      var op_params = []
      for(var key in options.data){
        op_params.push([key, options.data[key]].join('='));
      }
      var params = options.params;
      return '/rails/info?' + op_params.join('&');
    }else if(options && options.params) {
      var params = options.params;
      return '/rails/info'
    }else {
      var params = options;
      return '/rails/info'
    }
  }
