class ItemListingPolicy
  attr_reader :user, :item_listing

  def initialize(user, item_listing)
    @user = user
    @item_listing = item_listing
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def new?
    create?
  end

  def update?
    return false if user_is_invalid?
    return true if user.admin?

    return false # only admins can edit auctions

    #return false unless user_owns_listing?
    #return (!@user.nil? && user.admin?) #or @item_listing.item_listing_status_id == ItemListingStatus::LISTED
  end

  def edit?
    update?
  end

  def destroy?
    return false if user_is_invalid?
    return true if user.admin?

    return false # only admins can delete auctions

    #return !@user.nil? && user.admin?
  end

  def bid?
    return false if user_is_invalid?
    return true if user.admin?

    return false if user_owns_listing?
    return false if user_is_high_bidder?

    return (@item_listing.listed? or @item_listing.has_bids?)
  end

  def buy?
    return false if user_is_invalid?
    return true if user.admin?

    return false if user_owns_listing?

    return (@item_listing.listed? or @item_listing.has_bids?)
  end

  def offer?
    return false if user_is_invalid?
    return true if user.admin?

    return false if user_owns_listing?

    return (@item_listing.listed? or @item_listing.has_offers?)
  end

  def delivered?
    return false if user_is_invalid?
    return false if @item_listing.delivered? # Not even an admin can deliver an already delivered item...
    return true if user.admin?

    return false if (user_owns_listing? && @item_listing.buy?) # You can't deliver an item you are buying

    return true
  end

  def received?
    return false if user_is_invalid?
    return false if @item_listing.received? # Not even an admin can receive an already received item...
    return true if user.admin?

    return false if (user_owns_listing? && @item_listing.sell?) # You can't receive an item you are selling

    return true
  end

  def scope
    Pundit.policy_scope!(user, @item_listing.class)
  end

  private

  def user_owns_listing?
    @item_listing.user == @user
  end

  def user_is_invalid?
    @user.nil? or @user.accounts.nil? or @user.accounts.empty?
  end

  def user_is_high_bidder?
    return false if @item_listing.high_bidder_user.nil?

    @item_listing.high_bidder_user == @user
  end
end
