class AccountPolicy
  attr_reader :user, :account

  def initialize(user, account)
    @user = user
    @account = account
  end

  def index?
    true
  end

  def show?
    #scope.where(:id => @account.id).exists?
    true
  end

  def create?
    true
  end

  def new?
    create?
  end

  def update?
    user.admin?
  end

  def edit?
    update?
  end

  def destroy?
    user.admin?
  end

  def scope
    Pundit.policy_scope!(user, @account.class)
  end
end
