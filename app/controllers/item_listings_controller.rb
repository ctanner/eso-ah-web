class ItemListingsController < ApplicationController
  before_action :authenticate_user!, only: [:bid, :buy, :offer]
  before_action :set_item_listing, only: [:show, :bid, :buy, :offer]
  before_action :set_current_account, only: [:bid, :buy, :offer]

  after_action :verify_authorized, only: [:show, :bid, :buy, :offer]

  layout 'item_listings'

  add_breadcrumb "Home", :root_path
  add_breadcrumb "Auctions", :item_listings_path

  DEFAULT_SORTS = ['item_name asc', 'current_bid asc', 'buyout asc']

  # GET /item_listings
  # GET /item_listings.json
  def index
    redirect_to action: :wts
  end

  def wts
    ItemListing.expire_auctions

    if current_user.nil?
      @q = ItemListing.joins(item: [:item_type, :item_subtype], account: :server).wts.search(params[:q])
    else
      @q = ItemListing.joins(item: [:item_type, :item_subtype], account: :server).wts_for_server(selected_server_id).not_for_user(current_user).search(params[:q])
    end

    @q.sorts = DEFAULT_SORTS if @q.sorts.empty?
    @listings = @q.result.page(params[:page])

    add_breadcrumb "WTS", :wts_item_listings_path
  end

  def wtb
    ItemListing.expire_auctions

    if current_user.nil?
      @q = ItemListing.joins(item: [:item_type, :item_subtype], account: :server).wtb.search(params[:q])
    else
      @q = ItemListing.joins(item: [:item_type, :item_subtype], account: :server).wtb_for_server(selected_server_id).not_for_user(current_user).search(params[:q])
    end

    @q.sorts = DEFAULT_SORTS if @q.sorts.empty?
    @listings = @q.result.page(params[:page])

    add_breadcrumb "WTB", :wtb_item_listings_path
  end

  def wtt
    ItemListing.expire_auctions

    #@q = ItemListing.of_kind(ItemListingType::TRADE).search(params[:q])
    #@q.sorts = DEFAULT_SORTS if @q.sorts.empty?
    #@listings = @q.result.page(params[:page])

    add_breadcrumb "WTT", :wtt_item_listings_path
  end

  # Bid on an item offered for sale in /wts
  def bid
    authorize @item_listing

    if @current_account.nil?
      flash[:error] = "Please select an account"
      render :nothing => true
    else
      ItemListing.transaction do
        @item_listing.place_bid(current_user, @current_account)
        UserBid.create(user: current_user, account: @current_account, item_listing: @item_listing, quantity: @item_listing.quantity, bid_amount: @item_listing.current_bid)
      end

      flash[:notice] = 'Bid successful.'

      respond_to do |format|
        format.js
      end
    end
  end

  # Buy an item offered for sale in /wts
  def buy
    authorize @item_listing

    if @current_account.nil?
      flash[:error] = "Please select an account"
      render :nothing => true
    else
      ItemListing.transaction do
        @item_listing.purchase(current_user, @current_account)
        UserPurchase.create(user: current_user, account: @current_account, item_listing: @item_listing, quantity: @item_listing.quantity, cost: @item_listing.buyout)
      end

      flash[:notice] = 'Purchase successful.'

      respond_to do |format|
        format.js
      end
    end
  end

  # Offer an item to fill a request in /wtb or /wtt
  def offer
    authorize @item_listing

    if @current_account.nil?
      flash[:error] = "Please select an account"
      render :nothing => true
    else
      ItemListing.transaction do
        @item_listing.offer(current_user, @current_account)
        UserOffer.create(user: current_user, account: @current_account, item_listing: @item_listing, quantity: @item_listing.quantity)
      end

      flash[:notice] = 'Offer successful.'

      respond_to do |format|
        format.js
      end
    end
  end

  # GET /item_listings/1
  # GET /item_listings/1.json
  def show
    authorize @item_listing
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_listing
      @item_listing = ItemListing.find(params[:id])
    end

    def set_current_account
      @current_account = Account.find(selected_account_id) if selected_account_id.present?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_listing_params
      params.require(:item_listing).permit(:user_id, :account_id, :item_id, :quantity, :starting_bid, :bid_increment,
                                            :current_bid, :buyout, :expiration_date, :item_listing_type_id, :item_listing_status_id,
                                            :cancellation_reason_id, :high_bidder_user_id, :high_bidder_account_id,
                                            :consumer_user_id, :consumer_account_id)
    end
end
