class HomeController < ApplicationController
  add_breadcrumb "Home", :root_path

  def index
  end

  def about
  end

  def contact
  end

  def privacy
  end
end
