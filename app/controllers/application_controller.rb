class ApplicationController < ActionController::Base
  include Pundit

  before_action :configure_permitted_parameters, if: :devise_controller?


  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  after_filter :flash_to_headers

  def flash_to_headers
    return unless request.xhr?

    response.headers['X-Message'] = flash_message unless flash_message.nil?
    response.headers["X-Message-Type"] = flash_type.to_s unless flash_type.nil?

    flash.discard # don't want the flash to appear when you reload page
  end

  def selected_server_id
    session[:selected_server_id]
  end

  def selected_account_id
    session[:selected_account_id]
  end

  helper_method :selected_account_id
  helper_method :selected_server_id

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :password_confirmation, :residence_country, accounts_attributes: [:server_id, :account_key]) }
  end

  private

  def flash_message
    [:error, :warning, :notice].each do |type|
      return flash[type] unless flash[type].blank?
    end

    return nil
  end

  def flash_type
    [:error, :warning, :notice].each do |type|
      return type unless flash[type].blank?
    end

    return nil
  end
end
