class ItemsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show, :update_subtypes]
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  after_action :verify_authorized, except: [:index, :show, :update_subtypes, :autocomplete, :update_form]

  add_breadcrumb "Home", :root_path
  add_breadcrumb "Items", :items_path

  # GET /items
  # GET /items.json
  def index
    @q = Item.search(params[:q])
    @items = @q.result.page(params[:page])
  end

  # GET /items/1
  # GET /items/1.json
  def show
  end

  def autocomplete
    @autocomplete_list = Item.name_like(params[:q]).select(:id, :name)

    render json: @autocomplete_list
  end

  def update_form
    if params[:select_item_id].present?
      @item = Item.find(params[:select_item_id])
      @subtypes = @item.item_type.item_subtypes.map {|st| [st.name, st.id]}.insert(0, "")
    else
      @item = Item.new
      @subtypes = []
    end

    respond_to do |format|
      format.js
    end
  end

  # GET /items/update_subtypes/1
  def update_subtypes
    if params[:type_id].present?
      @subtypes = ItemSubtype.where(enabled: true).for_type(params[:type_id]).map {|st| [st.name, st.id]}.insert(0, "")
    else
      @subtypes = []
    end

    respond_to do |format|
      format.js
    end
  end

  # GET /items/new
  def new
    @item = Item.new

    authorize @item

    add_breadcrumb "Create", :new_item_path
  end

  # GET /items/1/edit
  def edit
    authorize @item

    add_breadcrumb "Edit", :edit_item_path
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)

    @item.user = current_user

    authorize @item

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    authorize @item

    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    authorize @item

    @item.destroy

    respond_to do |format|
      format.html { redirect_to items_url }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :user_id, :item_type_id, :item_subtype_id, :level, :veteran_level, :rarity_id,
                                    :style_id, :set_bonus_trait_id, :researchable_trait_id, :enchant_1_id, :enchant_2_id,
                                    :enchant_1_value, :enchant_1_value_2, :enchant_2_value, :enchant_2_value_2,
                                    :damage, :armor)
    end
end
