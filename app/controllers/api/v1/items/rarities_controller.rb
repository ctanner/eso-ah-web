module Api
  module V1
    module Items
      class RaritiesController < ApplicationController
        respond_to :json

        def index
          @item_rarities = ItemRarity.all
        end

        def show
          @item_rarity = ItemRarity.find(params[:id])
        end
      end
    end
  end
end
