module Api
  module V1
    module Items
      class TiersController < ApplicationController
        respond_to :json

        def index
          @item_tiers = ItemTier.all
        end

        def show
          @item_tier = ItemTier.find(params[:id])
        end
      end
    end
  end
end
