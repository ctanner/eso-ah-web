module Api
  module V1
    module Items
      class ConfigurationsController < ApplicationController
        respond_to :json

        def index
          @item_configurations = ItemConfiguration.all
        end

        def show
          @item_configuration = ItemConfiguration.find(params[:id])
        end
      end
    end
  end
end
