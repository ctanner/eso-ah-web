module Api
  module V1
    module Items
      class EnchantsController < ApplicationController
        respond_to :json

        def index
          @item_enchants = ItemEnchant.all
        end

        def show
          @item_enchant = ItemEnchant.find(params[:id])
        end
      end
    end
  end
end
