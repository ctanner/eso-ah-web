module Api
  module V1
    module Items
      class StylesController < ApplicationController
        respond_to :json

        def index
          @item_styles = ItemStyle.all
        end

        def show
          @item_style = ItemStyle.find(params[:id])
        end
      end
    end
  end
end
