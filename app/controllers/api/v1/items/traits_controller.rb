module Api
  module V1
    module Items
      class TraitsController < ApplicationController
        respond_to :json

        def index
          @item_traits = ItemTrait.all
        end

        def show
          @item_trait = ItemTrait.find(params[:id])
        end
      end
    end
  end
end
