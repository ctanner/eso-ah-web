module Api
  module V1
    module Items
      class SetBonusesController < ApplicationController
        respond_to :json

        def index
          @item_set_bonuses = ItemSetBonus.all
        end

        def show
          @item_set_bonus = ItemSetBonus.find(params[:id])
        end
      end
    end
  end
end
