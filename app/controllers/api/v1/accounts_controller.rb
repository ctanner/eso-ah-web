module Api
  module V1
    class AccountsController < ApplicationController
      respond_to :json

      def index
        @accounts = Account.all
      end

      def show
        @account = Account.find(params[:id])
      end
    end
  end
end
