class Users::AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account, only: [:show, :edit, :update, :destroy]

  after_action :verify_authorized, except: [:index, :select]

  layout 'users/profile'

  add_breadcrumb "Home", :root_path
  add_breadcrumb "User", :user_profile_path
  add_breadcrumb "Accounts", :user_accounts_path

  def select
    if params[:select_account_id].present?
      account = Account.find(params[:select_account_id])

      session[:selected_account_id] = account.id
      session[:selected_server_id] = account.server_id
    else
      session[:selected_account_id] = nil
      session[:selected_server_id] = nil
    end

    head :no_content
  end

  # GET /accounts
  # GET /accounts.json
  def index
    @q = Account.for_user(current_user).search(params[:q])
    @q.sorts = ['server_name asc', 'account_key asc'] if @q.sorts.empty?
    @accounts = @q.result.page(params[:page])
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
    authorize @account
  end

  # GET /accounts/new
  def new
    @account = Account.new

    authorize @account

    add_breadcrumb "Create", :new_user_account_path
  end

  # GET /accounts/1/edit
  def edit
    authorize @account

    add_breadcrumb "Edit", :edit_user_account_path
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)

    authorize @account

    respond_to do |format|
      if @account.save
        format.html { redirect_to user_account_url(current_user, @account), notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    authorize @account

    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to user_account_url(current_user, @account), notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    authorize @account

    @account.destroy
    respond_to do |format|
      format.html { redirect_to user_accounts_url(current_user) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:user_id, :account_key, :server_id, :validated)
    end
end
