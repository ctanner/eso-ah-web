class Users::ItemListingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_item_listing, only: [:show, :edit, :update, :destroy, :delivered, :received]
  before_action :set_current_account, only: [:delivered, :received, :create]

  after_action :verify_authorized, only: [:show, :edit, :update, :destroy, :delivered, :received]

  add_breadcrumb "Home", :root_path
  add_breadcrumb "User", :user_profile_path
  add_breadcrumb "Auctions", :user_item_listings_path

  # GET /item_listings
  # GET /item_listings.json
  def index
    @q = ItemListing.active.for_user(current_user).search(params[:q])
    @item_listings = @q.result.page(params[:page])

    @q.sorts = ['item_listing_status_name asc'] if @q.sorts.empty?

    @cancellation_reasons = CancellationReason.all
  end

  def sales
    @q = ItemListing.sold_by_user(current_user).search(params[:q])
    @item_listings = @q.result.page(params[:page])

    @q.sorts = ['item_listing_status_name asc'] if @q.sorts.empty?

    @cancellation_reasons = CancellationReason.all

    add_breadcrumb "Sales", :sales_user_item_listings_path
  end

  def purchases
    @q = ItemListing.purchased_by_user(current_user).search(params[:q])
    @item_listings = @q.result.page(params[:page])

    @q.sorts = ['item_listing_status_name asc'] if @q.sorts.empty?

    @cancellation_reasons = CancellationReason.all

    add_breadcrumb "Purchases", :purchases_user_item_listings_path
  end

  # GET /item_listings/1
  # GET /item_listings/1.json
  def show
    authorize @item_listing
  end

  # GET /item_listings/new
  def new
    @item_listing = ItemListing.new
    @item_listing.item = Item.new

    add_breadcrumb "Create", :new_user_item_listing_path
  end

  # GET /item_listings/1/edit
  def edit
    authorize @item_listing

    add_breadcrumb "Edit", :edit_user_item_listing_path
  end

  # POST /item_listings
  # POST /item_listings.json
  def create
    @item_listing = ItemListing.new(item_listing_params)

    @item_listing.expiration_date = 3.days.from_now if @item_listing.expiration_date.nil?
    @item_listing.current_bid = @item_listing.starting_bid
    @item_listing.item_listing_status = ItemListingStatus.find(ItemListingStatus::LISTED)

    db_item = Item.where(
      name: @item_listing.item.name,
      item_type_id: @item_listing.item.item_type_id,
      item_subtype_id: @item_listing.item.item_subtype_id,
      rarity_id: @item_listing.item.rarity_id,
      style_id: @item_listing.item.style_id,
      level: @item_listing.item.level,
      veteran_level: @item_listing.item.veteran_level,
      set_bonus_trait_id: @item_listing.item.set_bonus_trait_id,
      researchable_trait_id: @item_listing.item.researchable_trait_id,
      enchant_1_id: @item_listing.item.enchant_1_id,
      enchant_2_id: @item_listing.item.enchant_2_id
      ).first

    @item_listing.item = db_item if db_item

    respond_to do |format|
      if @item_listing.save
        format.html { redirect_to user_item_listings_url(current_user), notice: 'Item listing was successfully created.' }
        format.json { render :show, status: :created, location: @item_listing }
      else
        format.html { render :new }
        format.json { render json: @item_listing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /item_listings/1
  # PATCH/PUT /item_listings/1.json
  def update
    authorize @item_listing

    respond_to do |format|
      if @item_listing.update(item_listing_params)
        format.html { redirect_to user_item_listing_url(current_user, @item_listing), notice: 'Item listing was successfully updated.' }
        format.json { render :show, status: :ok, location: @item_listing }
      else
        format.html { render :edit }
        format.json { render json: @item_listing.errors, status: :unprocessable_entity }
      end
    end
  end

  def cancel
    cancellation_params[:listings].each do |item_listing_id|
      item_listing = ItemListing.find(item_listing_id)

      item_listing.cancel(cancellation_params[:reason])
    end

    respond_to do |format|
      format.html { redirect_to user_item_listings_url, notice: 'Listings were successfully cancelled.' }
      format.js   { render :nothing => true }
    end
  end

  def delivered
    authorize @item_listing

    if @current_account.nil?
      flash[:error] = "Please select an account"
      render nothing: true
    else
      ItemListing.transaction do
        @item_listing.delivered
        UserDelivery.create(user: current_user, account: @current_account, item_listing: @item_listing)

        @item_listing.consumer_account.update(validated: true)
      end

      #flash[:notice] = 'Delivery successful.'

      respond_to do |format|
        format.js
      end
    end
  end

  def received
    authorize @item_listing

    if @current_account.nil?
      flash[:error] = "Please select an account"
      render nothing: true
    else
      ItemListing.transaction do
        @item_listing.received
        UserReceived.create(user: current_user, account: @current_account, item_listing: @item_listing)

        @item_listing.consumer_account.update(validated: true)
      end

      #flash[:notice] = 'Receipt successful.'

      respond_to do |format|
        format.js
      end
    end
  end

  # DELETE /item_listings/1
  # DELETE /item_listings/1.json
  def destroy
    authorize @item_listing

    @item_listing.destroy

    respond_to do |format|
      format.html { redirect_to user_item_listings_url(current_user) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_listing
      @item_listing = ItemListing.find(params[:id])
    end

    def set_current_account
      @current_account = Account.find(selected_account_id) if selected_account_id.present?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_listing_params
      params.require(:item_listing).permit(:user_id, :account_id, :item_id, :quantity, :starting_bid, :bid_increment,
                                            :current_bid, :buyout, :expiration_date, :item_listing_type_id, :item_listing_status_id,
                                            :cancellation_reason_id, :high_bidder_user_id, :high_bidder_account_id,
                                            :consumer_user_id, :consumer_account_id,
                                            item_attributes: [
                                              :user_id, :name, :item_type_id, :item_subtype_id, :rarity_id, :style_id, :level,
                                              :veteran_level, :set_bonus_trait_id, :researchable_trait_id,
                                              :enchant_1_id, :enchant_2_id, :enchant_1_value, :enchant_1_value_2, :enchant_2_value, :enchant_2_value_2,
                                              :damage, :armor
                                              ])

    end

    def cancellation_params
      params.require(:cancellation)
    end
end
