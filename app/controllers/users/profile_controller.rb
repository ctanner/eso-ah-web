class Users::ProfileController < ApplicationController
  before_action :authenticate_user!
  #after_action :verify_authorized

  layout 'users/profile'

  add_breadcrumb "Home", :root_path
  add_breadcrumb "User Profile", :user_profile_path

  def show
    @user = current_user
  end
end
