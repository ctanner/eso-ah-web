class Users::RegistrationsController < Devise::RegistrationsController
  def new
    respond_with build_resource.accounts.build
  end

  def create
    super
  end
end
