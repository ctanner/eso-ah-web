json.array!(@items) do |item|
  json.extract! item, :id, :kind, :name, :quest
  json.url item_url(item, format: :json)
end
