json.array!(@item_enchants) do |item_enchant|
  json.extract! item_enchant, :id, :name, :description
  json.url api_v1_items_enchant_url(item_enchant, format: :json)
end
