json.array!(@item_rarities) do |item_rarity|
  json.extract! item_rarity, :id, :name, :description
  json.url api_v1_items_rarity_url(item_rarity, format: :json)
end
