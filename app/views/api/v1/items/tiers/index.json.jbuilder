json.array!(@item_tiers) do |item_tier|
  json.extract! item_tier, :id, :name, :description
  json.url api_v1_items_tier_url(item_tier, format: :json)
end
