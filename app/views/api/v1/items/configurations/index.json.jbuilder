json.array!(@item_configurations) do |item_configuration|
  json.extract! item_configuration, :id, :name, :item_rarity_id, :item_style_id, :item_tier_id, :item_trait_1_id, :item_trait_2_id, :item_enchant_1_id, :item_enchant_2_id
  json.url api_v1_items_configuration_url(item_configuration, format: :json)
end
