json.array!(@item_traits) do |item_trait|
  json.extract! item_trait, :id, :name, :description, :item_set_bonus_id
  json.url api_v1_items_trait_url(item_trait, format: :json)
end
