json.array!(@item_styles) do |item_style|
  json.extract! item_style, :id, :name, :description
  json.url api_v1_items_style_url(item_style, format: :json)
end
