json.array!(@item_set_bonuses) do |item_set_bonus|
  json.extract! item_set_bonus, :id, :name, :description
  json.url api_v1_items_set_bonus_url(item_set_bonus, format: :json)
end
