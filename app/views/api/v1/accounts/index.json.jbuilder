json.array!(@accounts) do |account|
  json.extract! account, :id, :user_id, :account_key, :server_name, :validated
  json.url account_url(account, format: :json)
end
