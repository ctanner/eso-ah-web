=begin
class ItemSearch < Searchlight::Search
  search_on Item.all

  searches :name_like

  def search_name_like
    search.merge(Item.name_like(name_like))
  end
end
=end
