class ItemType < ActiveRecord::Base
  has_many :items
  has_many :item_subtypes
end
