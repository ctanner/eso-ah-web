class User < ActiveRecord::Base
  has_many :accounts, inverse_of: :user
  has_many :item_listings, inverse_of: :user

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  accepts_nested_attributes_for :accounts
end
