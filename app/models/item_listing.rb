class ItemListing < ActiveRecord::Base
  belongs_to :user, inverse_of: :item_listings
  belongs_to :high_bidder_user, :class_name => "User"
  belongs_to :high_bidder_account, :class_name => "Account"
  belongs_to :consumer_user, :class_name => "User"
  belongs_to :consumer_account, :class_name => "Account"
  belongs_to :account
  belongs_to :item, inverse_of: :item_listings
  belongs_to :item_listing_type
  belongs_to :item_listing_status
  belongs_to :cancellation_reason

  has_many :user_item_listing_purchases

  accepts_nested_attributes_for :item

  # Validations
  validates :user, :account, :item_listing_type, :item, presence: true
  validates :quantity, :expiration_date, presence: true
  validates :quantity, numericality: { greater_than: 0, only_integer: true }
  validates :starting_bid, :buyout, numericality: { greater_than: 0, only_integer: true }, allow_nil: true

  validate :starting_bid_or_buyout_must_be_present

  def starting_bid_or_buyout_must_be_present
    unless starting_bid.present? or buyout.present?
      errors.add(:buyout, "Please enter a buyout or starting bid")
      errors.add(:starting_bid, "Please enter a buyout or starting bid")
    end
  end

  # Scopes
  default_scope { where.not(item_listing_status_id: [ItemListingStatus::CANCELLED, ItemListingStatus::EXPIRED]) }

  scope :active, -> {
    where.not(item_listing_status_id: [ItemListingStatus::RECEIVED, ItemListingStatus::DELIVERED, ItemListingStatus::OFFER_ACCEPTED, ItemListingStatus::BOUGHT])
  }

  scope :wts, -> {
    where(item_listing_type_id: ItemListingType::SELL,
          item_listing_status_id: [ItemListingStatus::LISTED, ItemListingStatus::HAS_BIDS])
  }

  scope :wtb, -> {
    where(item_listing_type_id: ItemListingType::BUY,
          item_listing_status_id: [ItemListingStatus::LISTED, ItemListingStatus::HAS_OFFERS])
  }

  scope :for_server, -> server_id {
    joins(:account).where(accounts: { server_id: server_id })
  }

  scope :wts_for_server, -> server_id {
    if server_id.nil?
      wts
    else
      wts.for_server(server_id)
    end
  }

  scope :wtb_for_server, -> server_id {
    if server_id.nil?
      wtb
    else
      wtb.for_server(server_id)
    end
  }

  scope :sold_by_user, -> user {
    where(
      "(user_id = :user and item_listing_type_id = :sell and (item_listing_status_id = :bought or item_listing_status_id = :delivered)) or
        (consumer_user_id = :user and item_listing_type_id = :buy and (item_listing_status_id = :offer_accepted or item_listing_status_id = :delivered))", {
      user: user.id,
      sell: ItemListingType::SELL,
      buy: ItemListingType::BUY,
      bought: ItemListingStatus::BOUGHT,
      offer_accepted: ItemListingStatus::OFFER_ACCEPTED,
      delivered: ItemListingStatus::DELIVERED
      })
  }

  scope :purchased_by_user, -> user {
    where(
      "(consumer_user_id = :user and item_listing_type_id = :sell and (item_listing_status_id = :bought or item_listing_status_id = :delivered)) or
        (user_id = :user and item_listing_type_id = :buy and (item_listing_status_id = :offer_accepted or item_listing_status_id = :delivered))", {
      user: user.id,
      sell: ItemListingType::SELL,
      buy: ItemListingType::BUY,
      bought: ItemListingStatus::BOUGHT,
      offer_accepted: ItemListingStatus::OFFER_ACCEPTED,
      delivered: ItemListingStatus::DELIVERED
      })
  }

  scope :of_kind, -> type { where(item_listing_type_id: type) }
  scope :not_of_kind, -> type { where.not(item_listing_type_id: type) }

  scope :of_status, -> status { where(item_listing_status_id: status) }
  scope :not_of_status, -> status { where.not(item_listing_status_id: status) }

  scope :for_user, -> user { where(user_id: user.id) }
  scope :not_for_user, -> user { where.not(user_id: user.id) }

  def buy?
    item_listing_type_id == ItemListingType::BUY
  end

  def sell?
    item_listing_type_id == ItemListingType::SELL
  end

  def trade?
    item_listing_type_id == ItemListingType::TRADE
  end

  def listed?
    item_listing_status_id == ItemListingStatus::LISTED
  end

  def has_bids?
    item_listing_status_id == ItemListingStatus::HAS_BIDS
  end

  def bought?
    item_listing_status_id == ItemListingStatus::BOUGHT
  end

  def has_offers?
    item_listing_status_id == ItemListingStatus::HAS_OFFERS
  end

  def offer_accepted?
    item_listing_status_id == ItemListingStatus::OFFER_ACCEPTED
  end

  def delivered?
    item_listing_status_id == ItemListingStatus::DELIVERED
  end

  def received?
    item_listing_status_id == ItemListingStatus::RECEIVED
  end

  def cancelled?
    item_listing_status_id == ItemListingStatus::CANCELLED
  end

  def expired?
    item_listing_status_id == ItemListingStatus::EXPIRED
  end

  def cancel(reason_id)
    update(item_listing_status_id: ItemListingStatus::CANCELLED, cancellation_reason_id: reason_id)
  end

  def place_bid(user, account)
    update(current_bid: (current_bid || starting_bid || 0) + bid_increment, item_listing_status_id: ItemListingStatus::HAS_BIDS, high_bidder_user: user, high_bidder_account: account)
  end

  def purchase(user, account)
    update(item_listing_status_id: ItemListingStatus::BOUGHT, consumer_user: user, consumer_account: account)
  end

  def offer(user, account)
    #update(item_listing_status_id: ItemListingStatus::HAS_OFFERS)
    update(item_listing_status_id: ItemListingStatus::OFFER_ACCEPTED, consumer_user: user, consumer_account: account)
  end

  def offer_accepted(offer)
    update(item_listing_status_id: ItemListingStatus::OFFER_ACCEPTED, consumer_user: offer.user, consumer_account: offer.account)
  end

  def delivered
    update(item_listing_status_id: ItemListingStatus::DELIVERED)
  end

  def received
    update(item_listing_status_id: ItemListingStatus::RECEIVED)
  end

  def self.expire_auctions
    expired = ItemListing.where("expiration_date < now()").where(item_listing_status_id: [ItemListingStatus::LISTED, ItemListingStatus::HAS_BIDS])

    expired.each do |auction|
      case auction.item_listing_status_id
      when ItemListingStatus::LISTED
        auction.update!(item_listing_status_id: ItemListingStatus::EXPIRED)
      when ItemListingStatus::HAS_BIDS
        ItemListing.transaction do
          auction.update!(item_listing_status_id: ItemListingStatus::BOUGHT, consumer_user_id: auction.high_bidder_user_id, consumer_account_id: auction.high_bidder_account_id)
          UserPurchase.create(user: auction.high_bidder_user, account: auction.high_bidder_account, item_listing: auction, quantity: auction.quantity, cost: auction.current_bid)
        end
      end
    end
  end
end
