class UserBid < ActiveRecord::Base
  belongs_to :user
  belongs_to :account
  belongs_to :item_listing
end
