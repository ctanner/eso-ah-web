class ItemListingType < ActiveRecord::Base
  has_many :item_listings

  BUY = 1
  SELL = 2
  TRADE = 3
end
