class Account < ActiveRecord::Base
  belongs_to :user, inverse_of: :accounts
  belongs_to :server

  has_many :item_listings

  validates :user, :server, :account_key, presence: true

  scope :for_user, -> user { where(user_id: user.id) }

  def name
    "#{server.name} - #{account_key}"
  end
end
