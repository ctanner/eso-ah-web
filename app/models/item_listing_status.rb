class ItemListingStatus < ActiveRecord::Base
  has_many :item_listings

  LISTED = 1
  HAS_BIDS = 2
  BOUGHT = 3
  HAS_OFFERS = 4
  OFFER_ACCEPTED = 5
  DELIVERED = 6
  RECEIVED = 7
  CANCELLED = 8
  EXPIRED = 9
end
