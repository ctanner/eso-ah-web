class Enchant < ActiveRecord::Base
  validates :name, presence: true
end
