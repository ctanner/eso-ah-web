class ResearchableTrait < ActiveRecord::Base
  has_many :items

  validates :name, :description, presence: true
end
