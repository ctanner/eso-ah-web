class Item < ActiveRecord::Base
  belongs_to :item_type
  belongs_to :item_subtype
  belongs_to :user

  belongs_to :rarity
  belongs_to :style
  belongs_to :set_bonus_trait
  belongs_to :researchable_trait
  belongs_to :enchant_1, :class_name => "Enchant"
  belongs_to :enchant_2, :class_name => "Enchant"

  has_many :item_listings, inverse_of: :item

  validates :name, :item_type, :item_subtype, presence: true

  validates :enchant_1_value, :enchant_1_value_2, :enchant_2_value, :enchant_2_value_2, numericality: { greater_than: 0, only_integer: true }, allow_nil: true

  validate :enchantment_values_required

  scope :name_like, -> name { where("name like ?", "%#{name}%")}

  def enchantment_values_required
    unless enchant_1.nil?
      errors.add(:enchant_1_value, "Please enter a value") unless enchant_1_value.present?
      errors.add(:enchant_1_value_2, "Please enter a duration/recovers value") if enchant_1.description.include? "%2" unless enchant_1_value_2.present?
    end

    unless enchant_2.nil?
      errors.add(:enchant_2_value, "Please enter a value") unless enchant_2_value.present?
      errors.add(:enchant_2_value_2, "Please enter a duration/recovers value") if enchant_2.description.include? "%2" unless enchant_2_value_2.present?
    end
  end
end
