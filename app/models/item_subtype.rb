class ItemSubtype < ActiveRecord::Base
  has_many :items
  belongs_to :item_type

  scope :for_type, -> item_type_id { where(item_type_id: item_type_id)}
end
